import './App.css';

import React, { Component } from 'react'

import { Route, Switch } from "react-router-dom";

import Header from './Components/Header/Header';

import Home from './home';

import Signup from './Components/signup';

import Products from './products';

import ProductDetails from './productDetails';

import ProductNotFound from './Components/ProductNotFound/ProductNotFound';


class App extends Component {

   render() {
      return (
         <div className='App'>
            <Header />
            <Switch>
               <Route exact path='/' component={Home} />
               <Route exact path='/signup' component={Signup} />
               <Route exact path='/products' component={Products} />
               <Route exact path='/products/:id' component={ProductDetails} />
               <Route component={ProductNotFound} />
            </Switch>
         </div>
      )
   }
}

export default App

      // <Route path='/signup' element={<Signup />} />
   // < Route path = '/products' element = {< Products />} />
   // < Route path = '/products/:id' element = {< ProductDetails />} />

   // < Route path = '/signup' element = {< Signup />} />
   //    < Route path = '/products' element = {< Products />} />
   //       < Route path = '/products/:id' element = {< ProductDetails />} />