import React from "react";
import { Link } from "react-router-dom";

import Fetchfail from "./Components/Fetchfail/Fetchfail";

import Spinner from "./Components/Spinner/Spinner";

import './Products.css'

class Products extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            products: [],
            isLoded: false,
            isError: null,
        }
    }

    componentDidMount() {
        fetch('https://fakestoreapi.com/products')
            .then(response => {
                if (!response.ok) {
                    throw Error('Failed to fetch')
                }
                return response.json()
            })
            .then(data => {
                this.setState({
                    products: data,
                    isLoded: true,
                })
            })
            .catch((err) => {
                this.setState({
                    isLoded: true,
                    isError: err.message
                })
            })
    }

    render() {
        const { products, isLoded, isError } = this.state
        if (!isLoded) {
            return (
                <div className="d-flex flex-column justify-content-center align-items-center m-auto pt-5 spinner-container-bg">
                    <h1 className="loadMsg">Loading... </h1>
                    <Spinner />
                </div>
            )
        } else {
            if (isError) {
                return (
                    <Fetchfail Error={isError} />
                )
            } else {
                if (products.length) {
                    return (
                        <div className="d-flex flex-column justify-content-center align-items-center">
                            <h1 className="m-5 product-heder">Welcome to Our Products</h1>
                            <div className="d-flex flex-wrap m-4 align-self-center">
                                {products.map(each => {
                                    return (
                                        <div key={each.id} className='d-flex flex-column justify-content-center product-container m-3 shadow p-3 mb-5 bg-white rounded'>
                                            <h3 className='product-name'>{each.title}</h3>
                                            <img src={each.image} alt="" className='product-image mb-2' />
                                            <p className="pt-2">Price : <span className="product-price">{each.price}</span></p>
                                            <p>To know more.. <Link className="font-weight-bold" to={`/products/${each.id}`}>click here</Link></p>
                                        </div>
                                    )
                                })}
                            </div>
                        </div>
                    )
                }
                else {
                    return (
                        <div className="d-flex justify-content-center align-items-center m-5 p-5">
                            <p>Oops! No products found now. We will update the products soon. Try after some time.</p>
                        </div>
                    )
                }
            }
        }
    }
}

export default Products