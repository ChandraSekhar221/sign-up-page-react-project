import React from 'react'

export default function SubmitButton(props) {
    const { handleEvent ,submitResponse} = props
    return (
        <div>
            <input type='submit' value='Click here to submit' className='btn btn-info' onClick={handleEvent}></input>
            {submitResponse ? <p className='successMsg'>Your Account is created Successfully...</p> : null}
        </div>
    )
}
