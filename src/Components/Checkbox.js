import React from 'react'

function Checkbox(props) {

    const { handleEvent, userValue,submitStatus } = props

    return (
        <div className='mb-2'>
            <div className='form-control border border-primary '>
                <input type='checkbox' id='checkbox' className='mr-3' onChange={handleEvent}></input>
                <label htmlFor='checkbox' className='m-0 p-0 font-weight-bold'>Accept terms and conditions</label>
            </div>
            {userValue === '' ? null : userValue === false ? <p className='errMsg m-0'>Accept the terms and constions</p> : null}
            {submitStatus === false ? <p className='errMsg m-0'>*Tick the Checkbox</p> : null}
        </div>
    )
}

export default Checkbox
