import React from 'react'

function RoleSelect({ handleEvent, userValue, submitStatus }) {
  return (
    <div className="form-group mb-2">
      <label htmlFor="role" className='h5 m-0 p-0 text-end font-weight-bold, label'>Select Role:</label>
      <select className="form-control border border-primary" id="role" onChange={handleEvent} >
        <option value='Select Role'>Select Role</option>
        <option value='Developer'>Developer</option>
        <option value='Senior Developer'>Senior Developer</option>
        <option value='Lead Engineer'>Lead Engineer</option>
        <option value='CTO'>CTO</option>
      </select>
      {userValue === 'Select Role' ? <p className='errMsg m-0'>*Select the Role.</p> : null}
      {submitStatus === false ? <p className='errMsg m-0'>*Select Your Role</p> : null}
    </div>
  )
}

export default RoleSelect
