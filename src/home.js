import React, { Component } from 'react'

import './home.css'

import { Link } from 'react-router-dom'

export class Home extends Component {
    render() {
        return (
            <div className='main-container-bg d-flex flex-column'>
                <section className='d-flex justify-content-center align-items-center text-center flex-grow-1'>
                    <div className='container'>
                        <div className='row'>
                            <div className='col'>
                                <h1 className='heading'>Welcome to Best Jobs World...</h1>
                                <p className='description'>Here are the highest paying jobs of 2022: Anesthesiologist: $208,000; Surgeon: $208,000; Obstetrician and Gynecologist: $208,000; Orthodontist: $208,000; Oral ...</p>
                                <p className='description'>To grab the oppurtunity <Link to='signup' className='signup-link'>Signup</Link> here.</p>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}

export default Home

        // <div className='row'>
        //     <div className='col'>
        //         <h2 className='pt-2 pb-2 title'>Welcome to You, Enrol your details to grab Industry best job...</h2>
        //     </div>
        // </div>