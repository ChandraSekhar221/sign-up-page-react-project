import React from 'react'

import image from './404-error.jpg'

import './ProductNotFound.css'

function ProductNotFound() {
    return (
        <div className='d-flex flex-column justify-content-center align-items-center bg-container text-center m-auto'>
            <img className='image' src={image} alt='' />
        </div>
    )
}

export default ProductNotFound
