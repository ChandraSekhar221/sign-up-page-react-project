import React, { Component } from 'react'

import validator from 'validator';

import './Form.css'

import Input from './Input'

import AgeInput from './AgeInput';

import GenderSelect from './GenderSelect'

import RoleSelect from './RoleSelect'

import EmailInput from './EmailInput'

import PasswordInput from './PasswordInput'

import RepeatPassword from './RepeatPassword';

import Checkbox from './Checkbox'

import SubmitButton from './SubmitButton'

class Form extends Component {

    constructor(props) {
        super(props)

        this.state = {
            firstName: '',
            lastName: '',
            age: '',
            gender: '',
            role: '',
            email: '',
            password: '',
            repeatPassword: '',
            checkboxStatus: '',
            submitStatus: {
                firstName: '',
                lastName: '',
                age: '',
                gender: '',
                role: '',
                email: '',
                password: '',
                repeatPassword: '',
                checkboxStatus: '',
            },
            submitResponse: false,
        }
        this.handleUserFName = this.handleUserFName.bind(this)
        this.handleUserLName = this.handleUserLName.bind(this)
        this.handleUserAge = this.handleUserAge.bind(this)
        this.handleGenderChanges = this.handleGenderChanges.bind(this)
        this.handleRoleSelect = this.handleRoleSelect.bind(this)
        this.handleUserEmail = this.handleUserEmail.bind(this)
        this.handleUserPassword = this.handleUserPassword.bind(this)
        this.handleRepeatpassword = this.handleRepeatpassword.bind(this)
        this.handleCheckbpxStatus = this.handleCheckbpxStatus.bind(this)
        this.handleSubmitStatus = this.handleSubmitStatus.bind(this)

        this.initialStateCopy = JSON.parse(JSON.stringify(this.state))
    }

    handleUserFName(e) {
        let userEnterdData = e.target.value.trim()
        if (userEnterdData.length > 0) {
            if (validator.isAlpha(userEnterdData)) {
                this.setState({
                    firstName: userEnterdData,
                    submitStatus: { ...this.state.submitStatus, firstName: true }
                })
            } else {
                this.setState({
                    firstName: false,
                })
            }
        }
        else {
            this.setState({
                firstName: ''
            })
        }
    }
    handleUserLName(e) {
        let userEnterdData = e.target.value.trim()
        if (userEnterdData.length > 0) {
            if (validator.isAlpha(userEnterdData)) {
                this.setState({
                    lastName: userEnterdData,
                    submitStatus: { ...this.state.submitStatus, lastName: true }
                })
            } else {
                this.setState({
                    lastName: false,
                })
            }
        }
        else {
            this.setState({
                lastName: ''
            })
        }
    }
    handleUserAge(e) {
        let userEnterdData = e.target.value.trim()
        if (userEnterdData.length > 0) {
            if (validator.isNumeric(userEnterdData) && parseFloat(userEnterdData) > 0 && parseFloat(userEnterdData) < 100 ) {
                this.setState({
                    age: userEnterdData,
                    submitStatus: { ...this.state.submitStatus, age: true },
                })
            } else {
                this.setState({
                    age: false,
                })
            }
        }
        else {
            this.setState({
                age: ''
            })
        }
    }
    handleGenderChanges(e) {
        this.setState({
            gender: e.target.value,
            submitStatus: { ...this.state.submitStatus, gender: true }
        })
    }
    handleRoleSelect(e) {
        this.setState({
            role: e.target.value,
            submitStatus: { ...this.state.submitStatus, role: true },
        })
    }
    handleUserEmail(e) {
        let userEnterdData = e.target.value.trim()
        if (userEnterdData.length > 0) {
            if (validator.isEmail(userEnterdData)) {
                this.setState({
                    email: userEnterdData,
                    submitStatus: { ...this.state.submitStatus, email: true },
                })
            } else {
                this.setState({
                    email: false,
                })
            }
        }
        else {
            this.setState({
                email: ''
            })
        }
    }
    handleUserPassword(e) {
        let userEnterdData = e.target.value.trim()
        if (userEnterdData.length > 0) {
            if (validator.isStrongPassword(userEnterdData)) {
                this.setState({
                    password: userEnterdData,
                    submitStatus: { ...this.state.submitStatus, password: true },
                })
            } else {
                this.setState({
                    password: false,
                })
            }
        }
        else {
            this.setState({
                password: ''
            })
        }
    }
    handleRepeatpassword(e) {
        let password = this.state.password;
        if (password === e.target.value) {
            this.setState({
                repeatPassword: e.target.value,
                submitStatus: { ...this.state.submitStatus, repeatPassword: true },
            })
        } else {
            this.setState({
                repeatPassword: false,
            })
        }
    }
    handleCheckbpxStatus(e) {
        this.setState({
            checkboxStatus: e.target.checked,
            submitStatus: { ...this.state.submitStatus, checkboxStatus: true },
        })
    }
    handleSubmitStatus(e) {
        e.preventDefault()
        this.setState({
            submitResponse: false,
        })
        let testObject = { ...this.state.submitStatus }
        for (let each in testObject) {
            if (!testObject[each]) {
                testObject[each] = false
            }
        }
        this.setState({
            submitStatus: { ...this.state.submitStatus, ...testObject }
        })
        if (Object.values(testObject).indexOf(false) === -1) {
            console.log(this.state)
            this.setState({
                ...this.initialStateCopy
            }, () => {
                this.setState({
                    submitResponse: true,
                }, () => {
                    e.target.parentElement.parentElement.reset()
                })
            })
        }
    }


    render() {
        const { firstName, lastName, age, gender, role, password, email, repeatPassword, checkboxStatus } = this.state
        return (
            <form className='p-2'>
                <Input label='FirstName' handleEvent={this.handleUserFName} userValue={firstName} submitStatus={this.state.submitStatus.firstName} />
                <Input label='LastName' handleEvent={this.handleUserLName} userValue={lastName} submitStatus={this.state.submitStatus.lastName} />
                <AgeInput label='Age' handleEvent={this.handleUserAge} userValue={age} submitStatus={this.state.submitStatus.age} />
                <GenderSelect handleEvent={this.handleGenderChanges} userValue={gender} submitStatus={this.state.submitStatus.gender} />
                <RoleSelect handleEvent={this.handleRoleSelect} userValue={role} submitStatus={this.state.submitStatus.role} />
                <EmailInput label='Email' handleEvent={this.handleUserEmail} userValue={email} submitStatus={this.state.submitStatus.email} />
                <PasswordInput label='Password' handleEvent={this.handleUserPassword} userValue={password} submitStatus={this.state.submitStatus.password} />
                <RepeatPassword label='Repeat Password' handleEvent={this.handleRepeatpassword} userValue={repeatPassword} submitStatus={this.state.submitStatus.repeatPassword} />
                <Checkbox handleEvent={this.handleCheckbpxStatus} userValue={checkboxStatus} submitStatus={this.state.submitStatus.checkboxStatus} />
                <SubmitButton handleEvent={this.handleSubmitStatus} submitResponse={this.state.submitResponse} />
            </form>
        )
    }
}

export default Form
