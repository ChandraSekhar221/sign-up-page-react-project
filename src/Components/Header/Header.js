import React from 'react'

import { Link } from 'react-router-dom'

function Header() {
    return (
        <header className="container-fluid p-0">
            <nav className="navbar navbar-expand-md navbar-dark bg-dark">
                <Link to='/' className='navbar-brand' >
                    <img src='logoImage.jpg' className='logo-image mr-3' alt='' />
                    BestJobs
                </Link>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse justify-content-center mr-5" id="navbarNavDropdown">
                    <ul className="navbar-nav">
                        <li className="nav-item active">
                            <Link to='/' className="nav-link navItem" aria-current="page">Home</Link>
                        </li>
                        <li className="nav-item">
                            <Link to='/products' className="nav-link navItem">Products</Link>
                        </li>
                        <li className="nav-item">
                        <Link to='/signup' className="nav-link navItem" aria-current="page">Signup</Link>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
    )
}

export default Header

    // <nav className = 'navbar navbar-dark navbar-expand-md bg-dark w-100' >
    //     <div className='container-fluid'>
    //         <Link to='/' className='navbar-brand' >
    //             <img src='logoImage.jpg' className='logo-image mr-3' alt='' />
    //             BestJobs
    //         </Link>
    //         <div className="collapse navbar-collapse navItem flex-grow-0 " id="navbarSupportedContent">
    //             <ul className="navbar-nav me-auto mb-2 mb-lg-0">
    //                 <li className="nav-item">
    //                     <Link to='/' className="nav-link navItem" aria-current="page">Home</Link>
    //                 </li>
    //                 <li className="nav-item">
    //                     <Link to='products' className="nav-link navItem">Products</Link>
    //                 </li>
    //                 <li className="nav-item">
    //                     <Link to='/' className="nav-link navItem" aria-current="page">Services</Link>
    //                 </li>
    //             </ul>
    //         </div>
    //         <Link to='/signup' ><button className='btn m-3'>Sign up</button></Link>
    //     </div>
    // </nav >
