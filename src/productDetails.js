import './ProductDetails.css'

import React, { Component } from 'react'

import Spinner from './Components/Spinner/Spinner'

import Fetchfail from './Components/Fetchfail/Fetchfail'

export class ProductDetails extends Component {

    constructor(props) {
        super(props)

        this.state = {
            product: {},
            isloaded: false,
            isError: false,
        }
    }

    componentDidMount() {
        let id = this.props.match.params.id
        fetch(`https://fakestoreapi.com/products/${id}`)
            .then(response => {
                if (!response.ok) {
                    throw Error('Failed to Fetch')
                }
                return response.json()
            })
            .then(data => {
                this.setState({
                    isloaded: true,
                    product: data,
                })
            })
            .catch((err) => {
                this.setState({
                    isloaded: true,
                    isError: err.message
                })
            })
    }

    render() {
        const { isloaded, product, isError } = this.state
        if (!isloaded) {
            return (
                <div className="d-flex flex-column justify-content-center align-items-center m-auto pt-5 spinner-container-bg">
                    <h1 className="loadMsg">Loading... </h1>
                    <Spinner />
                </div>
            )
        } else {
            if (isError) {
                return (
                    <Fetchfail Error={isError}/>
                )
            } else {
                return (
                    <div className='d-flex flex-column justify-content-center p-5 pdbg-container'>
                        <h2 className='title pt-5'>{product.title}</h2>
                        <img className='image2' src={product.image} alt='' />
                        <p className='pt-4 des'>{product.description}</p>
                        <p className='pt-2 pb font-weight-bold'> Category : {product.category}</p>
                        <p className='pt-2 font-weight-bold'> Price : {product.price}</p>
                        <p className='pt-2 font-weight-bold'>Rating : {product.rating.rate}</p>
                    </div>
                )
            }
        }
    }
}

export default ProductDetails





