import React from 'react'

function AgeInput(props) {
    let { label, handleEvent, userValue, submitStatus } = props
    return (
        <div className='form-group mb-2'>
            <label className='h5 m-0 p-0 text-end font-weight-bold label' htmlFor={label} >{label} :</label>
            <div className='logo-container d-flex border border-primary' >
                <input type='text' id={label} placeholder='Enter Here' onChange={handleEvent} className='no-border form-control p-1' ></input>
                <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" className="bi bi-person-fill" viewBox="0 0 16 16">
                    <path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z" />
                </svg>
            </div>
            {userValue === false ? <p className='errMsg m-0'>*Enter the Valid Age. The Age should only consist of the number and should be in between 0 and 100.</p> : null}
            {submitStatus === false ? <p className='errMsg m-0'>*Enter your age</p> : null}
        </div>
    )
}

export default AgeInput
