import React from 'react'

function GenderSelect({handleEvent,userValue,submitStatus}) {
    return (
        <div className="form-group mb-2">
            <label htmlFor="gender" className='h5 m-0 p-0 text-end font-weight-bold label'>Select Gender:</label>
            <select className="form-control border border-primary" id="gender" onChange={handleEvent} >
                <option value= 'Select Gender' >Select Gender</option>
                <option value='Male'>Male</option>
                <option value='Female' >Female</option>
            </select>
            {userValue === 'Select Gender' ? <p className='errMsg m-0'>*Select the Gender.</p> : null}
            {submitStatus === false ? <p className='errMsg m-0'>*Select Your Gender</p> : null}
        </div>
    )
}

export default GenderSelect
