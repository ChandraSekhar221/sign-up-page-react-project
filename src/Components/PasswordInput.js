import React from 'react'

function PasswordInput(props) {
    let { label, handleEvent, userValue, submitStatus } = props
    let id = label.toLowerCase()
    if (id.includes(' ')) {
        id = id.split(' ').join('')
    }
    return (
        <div className='form-group mb-2'>
            <label className='h5 m-0 p-0 text-end font-weight-bold label' htmlFor={id}>{label} :</label>
            <div className='logo-container d-flex border border-primary' >
                <input type='password' id={id} placeholder='Enter Here' onChange={handleEvent} className='no-border form-control p-1' ></input>
                <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" className="bi bi-key-fill" viewBox="0 0 16 16">
                    <path d="M3.5 11.5a3.5 3.5 0 1 1 3.163-5H14L15.5 8 14 9.5l-1-1-1 1-1-1-1 1-1-1-1 1H6.663a3.5 3.5 0 0 1-3.163 2zM2.5 9a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
                </svg>
            </div>
            {userValue === false ? <p className='errMsg m-0'>*Enter the valid Password. Password should contain at least one Capital letter, one small letter, one digit, one special char, and not less than 8 characters </p> : null}
            {submitStatus === false ? <p className='errMsg m-0'>*Enter Password</p> : null}
        </div>
    )
}

export default PasswordInput
