import React, { Component } from 'react'

import Form from './Form';

import pic from '../our-team.png'


export class Signup extends Component {
    render() {
        return (
            <div className="d-flex flex-column justify-content-center align-items-center pt-4">
                <header className="container">
                    <div className='row'>
                        <div className='col'>
                            <h2 className='pt-2 pb-2 title'>Sign up here...</h2>
                        </div>
                    </div>
                </header>
                <section className='container'>
                    <div className='row'>
                        <div className='col-11 col-md-6 form-container pt-3 pb-3 m-auto'>
                            <Form />
                        </div>
                        <div className='col-md-6 image-container d-none d-md-inline'>
                            <img src={pic} alt='' className='w-100 h-100' ></img>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}

export default Signup
