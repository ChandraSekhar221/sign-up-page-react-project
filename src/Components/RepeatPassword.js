import React from 'react'

function RepeatPassword(props) {
    let { label, handleEvent, userValue,submitStatus } = props
    let id = label.toLowerCase()
    if (id.includes(' ')) {
        id = id.split(' ').join('')
    }
    return (
        <div className='form-group mb-2'>
            <label className='h5 m-0 p-0 text-end font-weight-bold label' htmlFor={id}>{label} :</label>
            <div className='logo-container d-flex border border-primary' >
                <input type='password' id={id} placeholder='Enter Here' onChange={handleEvent} className='no-border form-control p-1' ></input>
                <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" className="bi bi-eye-slash-fill" viewBox="0 0 16 16">
                    <path d="m10.79 12.912-1.614-1.615a3.5 3.5 0 0 1-4.474-4.474l-2.06-2.06C.938 6.278 0 8 0 8s3 5.5 8 5.5a7.029 7.029 0 0 0 2.79-.588zM5.21 3.088A7.028 7.028 0 0 1 8 2.5c5 0 8 5.5 8 5.5s-.939 1.721-2.641 3.238l-2.062-2.062a3.5 3.5 0 0 0-4.474-4.474L5.21 3.089z" />
                    <path d="M5.525 7.646a2.5 2.5 0 0 0 2.829 2.829l-2.83-2.829zm4.95.708-2.829-2.83a2.5 2.5 0 0 1 2.829 2.829zm3.171 6-12-12 .708-.708 12 12-.708.708z" />
                </svg>
            </div>
            {userValue === false ? <p className='errMsg m-0'>*Password does not match. Re-enter the password.</p> : null}
            {submitStatus === false ? <p className='errMsg m-0'>*Repeat Password</p> : null}
        </div>
    )
}

export default RepeatPassword
